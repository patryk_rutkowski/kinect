﻿using LightBuzz.Vitruvius;
using Microsoft.Kinect;
using System.Windows;
using System;
using VitruviusTest.Pages;
using System.Windows.Navigation;

namespace VitruviusTest
{
    /// <summary>
    /// Logika interakcji dla klasy AppWindow.xaml
    /// </summary>
    public partial class AppWindow : Window
    {

        NavigationService service;

        private KinectSensor sensor;

        public AppWindow()
        {
            InitializeComponent();
            service = _NavigationFrame.NavigationService;
        }

        private void Window_Loaded(object sender, RoutedEventArgs e)
        {
            sensor = SensorExtensions.Default();
            sensor.EnableAllStreams();
            sensor.Start();
            service.Navigate(PrepareMenuPage());
        }

        private Menu PrepareMenuPage()
        {
            Menu menu = new Menu(sensor);
            menu.TutorialButtonHover += new EventHandler(pageMenu_TutorialButtonAction);
            menu.PlayButtonHover += new EventHandler(pageMenu_PlayButtonAction);
            menu.ExitButtonHover += new EventHandler(pageMenu_ExitButtonAction);
            return menu;
        }

        private void pageMenu_ExitButtonAction(object sender, EventArgs e)
        {
            System.Environment.Exit(2);
        }

        private void pageMenu_PlayButtonAction(object sender, EventArgs e)
        {
            service.Navigate(new Game(sensor));
        }

        public void pageMenu_TutorialButtonAction(object sender, EventArgs e)
        {
            Tutorial tutorial = new Tutorial(sensor);
            tutorial.TutorialEnds += new EventHandler(pageTutorial_TutorialEnds);
            service.Navigate(tutorial);
        }
        
        private void pageTutorial_TutorialEnds(object sender, EventArgs e)
        {
            service.Navigate(PrepareMenuPage());
        }
    }
    
}

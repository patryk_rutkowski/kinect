﻿using Microsoft.Kinect;
using System;
using System.Diagnostics;
using System.Linq;
using System.Windows;
using System.Windows.Controls;

namespace VitruviusTest
{
    /// <summary>
    /// Logika interakcji dla klasy Menu.xaml
    /// </summary>
    public partial class Menu : Page
    {
        private KinectSensor sensor;

        public event EventHandler TutorialButtonHover;
        public event EventHandler PlayButtonHover;
        public event EventHandler ExitButtonHover;

        private Stopwatch sw;
        private Image HoveredButton;
        private bool AllowToPressButton = true;

        public Menu(KinectSensor sensor)
        {
            this.sensor = sensor;
            InitializeComponent();
        }

        private void Page_Loaded(object sender, RoutedEventArgs e)
        {
            sensor.SkeletonFrameReady += Sensor_SkeletonFrameReady;
        }

        void Sensor_SkeletonFrameReady(object sender, SkeletonFrameReadyEventArgs e)
        {
            using (var frame = e.OpenSkeletonFrame())
            {
                if (frame != null)
                {
                    Skeleton[] bodies = new Skeleton[frame.SkeletonArrayLength];

                    frame.CopySkeletonDataTo(bodies);

                    var body = bodies.Where(b => b.TrackingState == SkeletonTrackingState.Tracked).FirstOrDefault();

                    if (body != null)
                    {
                        Joint handLeft = body.Joints[JointType.HandLeft];
                        Joint handRight = body.Joints[JointType.HandRight];

                        if (handLeft.TrackingState != JointTrackingState.NotTracked && handRight.TrackingState != JointTrackingState.NotTracked)
                        {
                            var activeHand = handRight;
                            var position = sensor.CoordinateMapper.MapSkeletonPointToColorPoint(activeHand.Position, ColorImageFormat.RgbResolution640x480Fps30);

                            cursor.Flip(activeHand);
                            cursor.Update(position);

                            if (CursorOnImageEvent(PlayButton) || CursorOnImageEvent(TutorialImage) || CursorOnImageEvent(ExitImage))
                            {
                                if (sw == null)
                                    sw = Stopwatch.StartNew();


                                ChoiceHoveredButton();
                                CheckButtonClick();
                            }
                            else
                            {
                                HoveredButton = null;
                                if (sw != null && sw.IsRunning)
                                {
                                    sw.Stop();
                                    sw = null;
                                }
                            }
                        }

                    }
                }
            }
        }

        private void ChoiceHoveredButton()
        {
            if (CursorOnImageEvent(PlayButton) && HoveredButton == null)
                HoveredButton = PlayButton;

            if (CursorOnImageEvent(TutorialImage) && HoveredButton == null)
                HoveredButton = TutorialImage;

            if (CursorOnImageEvent(ExitImage) && HoveredButton == null)
                HoveredButton = ExitImage;
        }
        
        private void CheckButtonClick()
        {
            if (sw.ElapsedMilliseconds > 2000)
            {
                if (CursorOnImageEvent(PlayButton) && HoveredButton == PlayButton && AllowToPressButton)
                {
                    AllowToPressButton = !AllowToPressButton;
                    OnPlayButtonHover();
                }

                if (CursorOnImageEvent(TutorialImage) && HoveredButton == TutorialImage && AllowToPressButton)
                {
                    AllowToPressButton = !AllowToPressButton;
                    OnTutorialButtonHover();
                }

                if (CursorOnImageEvent(ExitImage) && HoveredButton == ExitImage && AllowToPressButton)
                {
                    AllowToPressButton = !AllowToPressButton;
                    OnExitButtonHover();
                }
            }
            
        }

        private void OnPlayButtonHover()
        {
            if (PlayButtonHover != null)
            {
                PlayButtonHover(this, null);
            }
        }

        private void OnExitButtonHover()
        {
            if (ExitButtonHover != null)
            {
                ExitButtonHover(this, null);
            }
        }

        private void OnTutorialButtonHover()
        {
            if (TutorialButtonHover != null)
            {
                TutorialButtonHover(this, null);
            }
        }

        private Boolean CursorOnImageEvent(Image button)
        {
            return (Canvas.GetLeft(cursor) > Canvas.GetLeft(button) &&
                    Canvas.GetLeft(cursor) < Canvas.GetLeft(button) + button.ActualWidth &&
                    Canvas.GetTop(cursor) > Canvas.GetTop(button) &&
                    Canvas.GetTop(cursor) < Canvas.GetTop(button) + button.ActualHeight);
        }

    }
}

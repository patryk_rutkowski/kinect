﻿using LightBuzz.Vitruvius;
using Microsoft.Kinect;
using System.Linq;
using System.Windows.Controls;
using System;
using LightBuzz.Vitruvius.WPF;
using System.Diagnostics;

namespace VitruviusTest.Pages
{
    /// <summary>
    /// Logika interakcji dla klasy Tutorial.xaml
    /// </summary>
    public partial class Tutorial : Page
    {
        
        private const int TABS_COUNT = 5;

        public EventHandler TutorialEnds;

        private GestureController _gestureController;
        private KinectSensor sensor;

        public Tutorial(KinectSensor sensor)
        {
            this.sensor = sensor;
            InitializeComponent();
        }

        private void Page_Loaded(object sender, System.Windows.RoutedEventArgs e)
        {
            if (sensor != null)
            {
                sensor.SkeletonFrameReady += Sensor_SkeletonFrameReady;
                _gestureController = new GestureController(GestureType.Tutorial);
                _gestureController.GestureRecognized += GestureController_GestureRecognized;
            }
        }

        private void GestureController_GestureRecognized(object sender, GestureEventArgs e)
        {

            switch (e.Name)
            {
                case "SwipeLeft":
                    changeTab(tutorialTabs.SelectedIndex + 1);
                    break;
                case "SwipeRight":
                    changeTab(tutorialTabs.SelectedIndex - 1);
                    break;
                default:
                    break;
            }
        }

        private void changeTab(int index)
        {
            if (index < TABS_COUNT && index > -1)
            {
                tutorialTabs.SelectedIndex = index;
            }
            else if(index == TABS_COUNT)
            {
                OnTutorialsEnds();
            }
        }

        private void OnTutorialsEnds()
        {
            if (TutorialEnds != null)
                TutorialEnds(this, null);
        }

        private void Sensor_SkeletonFrameReady(object sender, SkeletonFrameReadyEventArgs e)
        {
            using (var frame = e.OpenSkeletonFrame())
                if (frame != null)
                {

                    var skeletons = frame.Skeletons().Where(s => s.TrackingState == SkeletonTrackingState.Tracked);

                    foreach (var skeleton in skeletons)
                        if (skeleton != null)
                        {
                            _gestureController.Update(skeleton);
                            break;
                        }
                }
        }

        private void Page_Unloaded(object sender, System.Windows.RoutedEventArgs e)
        {
            sensor.SkeletonFrameReady -= Sensor_SkeletonFrameReady;
        }
    }
}

﻿using LightBuzz.Vitruvius;
using LightBuzz.Vitruvius.WPF;
using Microsoft.Kinect;
using System.Linq;
using System.Windows;
using System.Windows.Controls;

namespace VitruviusTest
{
    /// <summary>
    /// Logika interakcji dla klasy Game.xaml
    /// </summary>
    public partial class Game : Page
    {
        #region --- Fields ---

        private GestureController _gestureControllerPlayer1;
        private GestureController _gestureControllerPlayer2;
        private KinectSensor sensor;
        private int Player1Gesture = 0;
        private int Player2Gesture = 0;

        #endregion

        #region --- Initialization ---

        public Game(KinectSensor sensor)
        {
            this.sensor = sensor;
            InitializeComponent();
            kinectViewer.FlippedHorizontally = true;
            kinectViewer.FlippedVertically = false;
        }

        private void Page_Loaded(object sender, RoutedEventArgs e)
        {
            if (sensor != null)
            {
                sensor.ColorFrameReady += Sensor_ColorFrameReady;
                sensor.DepthFrameReady += Sensor_DepthFrameReady;
                sensor.SkeletonFrameReady += Sensor_SkeletonFrameReady;

                _gestureControllerPlayer1 = new GestureController(GestureType.Game);
                _gestureControllerPlayer1.GestureRecognized += GestureController_GestureRecognized;

                _gestureControllerPlayer2 = new GestureController(GestureType.Game);
                _gestureControllerPlayer2.GestureRecognized += GestureController_GestureRecognized2;
            }
        }

        #endregion

        #region --- Properties ---

        public VisualizationMode Mode
        {
            get { return kinectViewer.FrameType; }
            set { kinectViewer.FrameType = value; }
        }

        #endregion

        #region --- Events ---

        private void Sensor_ColorFrameReady(object sender, ColorImageFrameReadyEventArgs e)
        {
            if (Mode != VisualizationMode.Color) return;

            using (var frame = e.OpenColorImageFrame())
                if (frame != null)
                    kinectViewer.Update(frame.ToBitmap());
        }

        private void Sensor_DepthFrameReady(object sender, DepthImageFrameReadyEventArgs e)
        {
            if (Mode != VisualizationMode.Depth) return;

            using (var frame = e.OpenDepthImageFrame())
                if (frame != null)
                    kinectViewer.Update(frame.ToBitmap());
        }

        private void Sensor_SkeletonFrameReady(object sender, SkeletonFrameReadyEventArgs e)
        {
            using (var frame = e.OpenSkeletonFrame())
                if (frame != null)
                {
                    kinectViewer.Clear();

                    tblHeights.Text = string.Empty;

                    var skeletons = frame.Skeletons().Where(s => s.TrackingState == SkeletonTrackingState.Tracked);

                    int i = 0;
                    test.Text = string.Format(i + "");


                    foreach (var skeleton in skeletons)
                        if (skeleton != null)
                        {
                            if (i == 0)
                                _gestureControllerPlayer1.Update(skeleton);

                            if (i == 1)
                                _gestureControllerPlayer2.Update(skeleton);

                            i++;
                            test.Text = string.Format(i + "");
                            // Draw skeleton
                            kinectViewer.DrawBody(skeleton);

                            // Display user height
                            tblHeights.Text += string.Format("\nUser {0}: {1}cm", skeleton.TrackingId, skeleton.Height());
                        }
                }
        }

        private void GestureController_GestureRecognized(object sender, GestureEventArgs e)
        {
            tblGestures.Text = "Player 1: " + e.Name;

            switch (e.Name)
            {
                case "TurnLeft":

                    break;
                case "TurnRight":

                    break;
                case "GoBackward":

                    break;
                case "GoForward":

                    break;
                default:
                    break;
            }
        }

        private void GestureController_GestureRecognized2(object sender, GestureEventArgs e)
        {
            tb2Gestures.Text = "Player 2: " + e.Name;

            switch (e.Name)
            {
                case "TurnLeft":

                    break;
                case "TurnRight":

                    break;
                case "GoBackward":

                    break;
                case "GoForward":

                    break;
                default:
                    break;
            }
        }

        private void Color_Click(object sender, RoutedEventArgs e)
        {
            Mode = VisualizationMode.Color;
        }

        private void Depth_Click(object sender, RoutedEventArgs e)
        {
            Mode = VisualizationMode.Depth;
        }

        #endregion

        private void Page_Unloaded(object sender, RoutedEventArgs e)
        {
            sensor.SkeletonFrameReady -= Sensor_SkeletonFrameReady;
            sensor.ColorFrameReady -= Sensor_ColorFrameReady;
            sensor.DepthFrameReady -= Sensor_DepthFrameReady;
        }
    }
}

﻿using System;
using Microsoft.Kinect;

namespace LightBuzz.Vitruvius.Gestures.Segments
{
    public class Stop : IGestureSegment
    {
        public GesturePartResult Update(Skeleton skeleton)
        {
            if (skeleton.Joints[JointType.HandRight].Position.Y < skeleton.Joints[JointType.HipCenter].Position.Y &&
                skeleton.Joints[JointType.HandLeft].Position.Y < skeleton.Joints[JointType.HipCenter].Position.Y)
            {
                return GesturePartResult.Succeeded;
            }

            return GesturePartResult.Failed;
        }
    }
}